#!/bin/bash
set -x
set -e

POD="$(oc get pods | grep Running | grep grafana-okd | awk '{print $1}')"

TMPDIR=$(mktemp -d)

cp -f scripts/grafana-migrate "${TMPDIR}/"
scp root@atomic-456.oit.duke.edu:/var/nfs/349-graphs/grafana/grafana.db \
    "${TMPDIR}/grafana.db"

sqlite3 "${TMPDIR}/grafana.db" 'DELETE FROM cache_data;'
sqlite3 "${TMPDIR}/grafana.db" 'VACUUM;'
oc rsh "${POD}" mkdir -p /tmp/migrate
oc rsync "${TMPDIR}/" "${POD}:/tmp/migrate"
oc rsh "${POD}" /tmp/migrate/grafana-migrate /tmp/migrate/grafana.db "postgres://$(oc get secret grafana-config -o json | jq .data.GF_DATABASE_USER -r | base64 -D):$(oc get secret grafana-config -o json | jq .data.GF_DATABASE_PASSWORD -r | base64 -D)@postgresql:5432/grafana?sslmode=disable"

echo "${TMPDIR}"
echo "${POD}"
