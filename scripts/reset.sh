#!/bin/bash
set -x

oc delete secret acme-account
oc delete secret grafana-config
oc delete secret postgresql
oc delete configmap grafana-config
oc delete all --all
oc delete pvc --all
oc delete sa openshift-acme
oc delete rolebinding openshift-acme
