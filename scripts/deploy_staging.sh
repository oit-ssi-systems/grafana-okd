#!/bin/bash

set -x

oc project graphs-staging && \
oc process -f okd/okd.template \
    GIT_BRANCH=staging \
    HOSTNAME=graphs-staging.oit.duke.edu \
    OIDC_CLIENT_ID="$(vault read -field client_id secret/linux/grafana/staging/oidc)" \
    OIDC_CLIENT_SECRET="$(vault read -field client_secret secret/linux/grafana/staging/oidc)" \
    | oc create -f -

until oc rollout status dc grafana-okd; do
         echo "Waiting..."
         sleep 5
     done
echo "Rolled out!"
