#!/bin/bash

set -x

oc project graphs-production && \
oc process -f okd/okd.template \
    GIT_BRANCH=master \
    HOSTNAME=graphs.oit.duke.edu \
    OIDC_CLIENT_ID="$(vault read -field client_id secret/linux/grafana/production/oidc)" \
    OIDC_CLIENT_SECRET="$(vault read -field client_secret secret/linux/grafana/production/oidc)" \
    | oc create -f -

until oc rollout status dc grafana-okd; do
         echo "Waiting..."
         sleep 5
     done
echo "Rolled out!"
